#include "../LArFecLvTempDcsTool.h"
#include "../LArHVToolMC.h" 
#include "../LArHVToolDB.h" 
#include "../LArFEBTempTool.h"
#include "../LArHVPathologyDbAlg.h"
#include "../LArHV2Ntuple.h"



DECLARE_COMPONENT( LArFecLvTempDcsTool )
DECLARE_COMPONENT( LArHVToolMC )
DECLARE_COMPONENT( LArHVToolDB )
DECLARE_COMPONENT( LArFEBTempTool )
DECLARE_COMPONENT( LArHVPathologyDbAlg )
DECLARE_COMPONENT( LArHV2Ntuple )

